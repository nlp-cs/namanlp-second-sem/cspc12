//
// Created by naman on 4/5/22.
//
// I. WAP to store a character string in a block of memory space and then modify the same to
//store a larger string.

#include <stdio.h>
#include <stdlib.h>

int main() {
    int sizePtr = 0;
    printf("Enter initial size of string : ");
    scanf(" %d", &sizePtr);
    char *ptr = (char *) malloc(sizePtr* sizeof(char));
    printf("Enter string : ");
    for(int i=0;i<sizePtr;i++)
        scanf(" %c",ptr+i);
    printf("String is : %s", ptr);

    // Reallocation of string

    printf("\nEnter final size of string : ");
    scanf(" %d", &sizePtr);

    ptr = (char *) realloc(ptr, sizePtr* sizeof(char));

    printf("Enter string : ");
    for(int i=0;i<sizePtr;i++)
        scanf(" %c",ptr+i);
    printf("New string is : %s", ptr);
}