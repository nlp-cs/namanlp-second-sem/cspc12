//
// Created by naman on 13/04/22.
//
// I. Implement a C program to Find Factorial of a Number

#include <stdio.h>

int main(){
    unsigned int number=0, factorial=1;
    printf("Enter the number to find factorial : ");
    scanf(" %d", &number);
    for(int i = 1; i<=number;i++)
        factorial*=i;
    printf("Factorial of given number is : %d", factorial);

}