//
// Created by naman on 20/04/22.
//
//  II. WAP with function to swap two integer arrays of same size using call by reference.

#include <stdio.h>

void swapArrays(long long int arr1[], long long int arr2[], int length){
    // swap each element
    for (int i = 0; i< length; i++){
        arr1[i] += arr2[i];
        arr2[i] = arr1[i] - arr2[i];
        arr1[i] -= arr2[i];
    }
}

int main(){
    int length = 0;
    printf("Enter length of arrays  : ");
    scanf("%d", &length);
    long long int array1[length], array2 [length];
    printf("Enter First Array : ");
    for(int i =0; i < length; i++) {
        scanf("%lld", &array1[i]);
    }
    printf("Enter Second Array : ");
    for(int i =0; i < length; i++)
        scanf("%lld", &array2[i]);

    printf("\nArray 1 before swapping :  ");
    for(int i =0; i < length; i++)
        printf("\t %lld", array1[i]);
    printf("\nArray 2 before swapping :  ");
    for(int i =0; i < length; i++)
        printf("\t %lld", array2[i]);

    swapArrays(array1, array2, length);

    printf("\nArray 1 after swapping :  ");
    for(int i =0; i < length; i++)
        printf("\t %lld", array1[i]);
    printf("\nArray 2 after swapping :  ");
    for(int i =0; i < length; i++)
        printf("\t %lld", array2[i]);

}