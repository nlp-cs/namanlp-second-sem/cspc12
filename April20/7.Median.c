//
// Created by naman on 23/04/22.
//

#include <stdio.h>

int main(){
    // Input array length and array
    int length=0;
    printf("Enter length of array  : ");
    scanf("%d", &length);
    long long int array1[length], array2[length];
    printf("Enter First sorted Array : ");
    for (int i=0; i <length;i++)
        scanf(" %lld", &array1[i]);
    printf("Enter Second sorted Array : ");
    for (int i=0; i <length;i++)
        scanf(" %lld", &array2[i]);

    long long int mergedArray[2*length]; // Merged array will be twice the size of each array

    // Add new element at i+jth position. New element is ith element of array1 or jth element of array2, whichever is smaller.

    for (int i = 0, j = 0; i+j < 2*length;){
        if ((array1[i] >= array2[j] && j<length) || i>=length){
            mergedArray[i+j] = array2[j];
            j++;
        } else{
            mergedArray[i+j] = array1[i];
            i++;
        }
    }
    printf("\n New merged array is : ");
    for (int  k=0; k< 2*length; k++)
        printf(" %lld ",mergedArray[k]);
    printf("\n Median of the merged array is : %f", (float)(mergedArray[length-1] + mergedArray[length])/2.0  );
}