//
// Created by naman on 20/04/22.
//
// IV. WAP with function to swap two strings using pointers.

#include <stdio.h>

void swapStrings(char **str1, char **str2){
    // swap pointer to string
    char *temp = *str1;
    *str1 = *str2;
    *str2 = temp;
}

int main(){
    char string1[100], string2[100];
    // input string

    printf("Enter String 1 (max 100 characters) : ");
    scanf(" %s", string1);
    printf("Enter String 2 (max 100 characters) : ");
    scanf(" %s", string2);

    printf("\nFirst string before swapping : %s", string1);
    printf("\nSecond string before swapping : %s", string2);

    swapStrings((char **) &string1,(char **) &string2);

    printf("\nFirst string before swapping : %s", string1);
    printf("\nSecond string before swapping : %s", string2);
}