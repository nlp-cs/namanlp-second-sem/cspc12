//
// Created by naman on 20/04/22.
//
// III. WAP to reverse an array by swapping (without using additional memory).

#include <stdio.h>

void reverseArrays(long long int arr1[], int length){
    for (int i = 0; i< length/2; i++){
        arr1[i] += arr1[length-i-1];
        arr1[length-i-1] = arr1[i] - arr1[length-i-1];
        arr1[i] -= arr1[length-i-1];
    }
}

int main(){
    int length = 0;
    printf("Enter length of arrays  : ");
    scanf("%d", &length);
    long long int array[length];

    printf("Enter Array : ");
    for(int i =0; i < length; i++) {
        scanf("%lld", &array[i]);
    }

    printf("\nArray reversing :  ");
    for(int i =0; i < length; i++)
        printf("\t %lld", array[i]);

    reverseArrays(array, length);

    printf("\nArray after reversing :  ");
    for(int i =0; i < length; i++)
        printf("\t %lld", array[i]);

}