//
// Created by naman on 27/4/22.
//
// 2. WAP to reverse multidimensional array as shown:
//[123]
//[456]
//[789]
//
//[321]
//[654]
//[987]

#include <stdio.h>

int main(){
    int rows = 0, columns = 0;
    printf("Enter number of rows and columns : ");
    scanf(" %d %d", &rows, &columns);
    int matrix[rows][columns];

    for (int row=0; row < rows; row++){
        printf("\nEnter row number %d : ", row);
        for (int column=0; column<columns; column++)
            scanf("%d", &matrix[row][column]);
    }

    for (int row = 0; row < rows; row++){
        for (int column = 0; column < columns/2; column++){
            int temp = matrix[row][column];
            matrix[row][column] = matrix[row][columns - column - 1];
            matrix[row][columns - column - 1] = temp;
        }
    }
    for (int row = 0; row < rows; row++){
        for (int column = 0; column < columns; column++)
            printf(" %d ", matrix[row][column] );
        printf("\n");
    }

}