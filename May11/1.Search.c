//
// Created by naman on 11/5/22.
//
//  I. Implement Linear search and Binary search on 1D array of Integers and also return the
//time complexity in all the cases i.e. your worst case, average case, and best case.

#include <stdio.h>

void linearSearch(const int array[], int length, int key){
    printf("\nLinear Search");
    for (int i = 0; i < length; i++ )
        if (array[i] == key){
            printf("\nElement found at position number %d\n", i+1);
            return;
        }
    printf("\nSorry, element not found\n");
}

void binarySearch(const int array[], int length, int key){
    printf("\nBinary Search");
    int found = 0, lowerLimit = 0, upperLimit = length-1;
    while (!found && lowerLimit <= upperLimit){
        int middle = lowerLimit + (upperLimit-lowerLimit)/2;
        if (array[middle] == key){
            printf("\nElement found at position number %d  \n", middle +1);
            found=1;
        } else if (array[middle] > key){
            upperLimit = middle - 1;
        } else
            lowerLimit = middle + 1;
    }
    if (!found)
        printf("\nElement not found\n");
}

int main(){

    int len = 0;
    printf("Enter number of elements in array : ");
    scanf(" %d", &len);
    int givenArray[len];
    printf("Enter array : ");
    for (int i = 0; i< len; i++)
        scanf(" %d", &givenArray[i]);
    printf("Enter element to search for : ");
    int key = 0;
    scanf(" %d", &key);
    linearSearch(givenArray, len, key);
    binarySearch(givenArray, len, key);
}