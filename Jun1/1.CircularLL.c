//
// Created by naman on 8/6/22.
//

#include <stdio.h>
#include <stdlib.h>

struct SCLL_Node{
    int data;
    struct  SCLL_Node * next;
};

// ---------------------------------------------------------- Get element data and key  ----------------------------------------------------------------------

struct SCLL_Node * getElement(){
    struct SCLL_Node * temp = (struct SCLL_Node * ) malloc(sizeof( struct  SCLL_Node));
    printf("\nEnter element : ");
    scanf(" %d", &(temp->data));
    return temp;
}

int getKey(){
    int key = 0;
    printf("\nEnter key : ");
    scanf(" %d", &key);
    return key;
}

// ---------------------------------------------------------- Find Size of Circular Linked List  ----------------------------------------------------------------------

int getSize(struct SCLL_Node * head){
    struct SCLL_Node * temp = head->next;
    int counter = 1;
    while(temp != head){
        counter++;
        temp = temp->next;
    }
    return counter;
}

// ---------------------------------------------------------- Accessing a node ----------------------------------------------------------------------

struct SCLL_Node * accessingNode(struct SCLL_Node * head, int key){

    // Actually returns previous element, to make it easier for further operations.

    struct SCLL_Node * traverse = head;
    int counter = 1;

    while (traverse->next->data != key){
        counter++;
        traverse = traverse->next;
        if (traverse->next == head)
            break;
    }

    if (head->data == key){
        printf("\nGiven key is head itself");
        return traverse;
    } else if(traverse->next == head && head->data != key){
        printf("\nKey, %d not found", key);
        return NULL;
    }

    printf("\nGiven key is %d ahead of head", counter);
    return traverse;
}


// ---------------------------------------------------------- Adding Node Before key ----------------------------------------------------------------------

void addNodeBefore(struct SCLL_Node * head,struct SCLL_Node * element, int key){
    struct SCLL_Node * traverse = accessingNode(head, key);

    if (traverse != NULL){
        struct SCLL_Node * temp = traverse->next;
        traverse->next = element;
        element->next = temp;
    }
}

// ---------------------------------------------------------- Adding Node After key ----------------------------------------------------------------------

void addNodeAfter(struct SCLL_Node * head,struct SCLL_Node * element, int key){
    struct SCLL_Node * traverse = accessingNode(head, key);

    if (traverse!=NULL){
        struct SCLL_Node * temp = traverse->next->next;
        traverse->next->next = element;
        element->next = temp;
    }
}

// ----------------------------------------------------------  Removing a node ----------------------------------------------------------------------

void removeNode(struct SCLL_Node * head, int key){
    struct SCLL_Node * traverse = accessingNode(head, key);

    if (getSize(head) <=2){
        printf("\nSorry, can not remove elements, as the size of list is %d", getSize(head));
        return;
    }

    if (traverse!=NULL){
        struct SCLL_Node * temp = traverse->next->next;
        free(traverse->next);
        traverse->next = temp;
        printf("\nNode successfully removed !");
    }
}

// ----------------------------------------------------------  Deleting complete list ----------------------------------------------------------------------

void deleteList(struct SCLL_Node * head){
    struct SCLL_Node * traverse = head;
    struct SCLL_Node * temp = head->next;
    while (temp != head){
        free(traverse);
        traverse = temp;
        temp = temp->next;
    }
}

// ---------------------------------------------------------- Displaying the list ----------------------------------------------------------------------

void displayList(struct SCLL_Node * head){
    struct SCLL_Node * traverse = head->next;
    printf("\n[ %d", head->data);
    do {
        printf(", %d", traverse->data);
        traverse = traverse->next;
    }while (traverse != head);

    printf(" ]");
}

// ----------------------------------------------------------  Sorting the List  ----------------------------------------------------------------------

//void swapNodes(struct SCLL_Node ** head_ptr, struct  SCLL_Node ** head_ptr2){
//    struct SCLL_Node * temp = *head_ptr;
//    **head_ptr = **head_ptr2;
//    **head_ptr2 = *temp;
//
//    temp = (*head_ptr)->next;
//    (*head_ptr)->next = (*head_ptr2)->next;
//    (*head_ptr2)->next = temp;
//}
//
////struct SCLL_Node * bubbleSort(struct SCLL_Node * head){
////
////}

// ----------------------------------------------------------  Magic Starts here ----------------------------------------------------------------------

int main(){
    struct SCLL_Node * head = (struct SCLL_Node * ) malloc(sizeof( struct  SCLL_Node));
    head->data = 10;
    head->next = (struct SCLL_Node * ) malloc(sizeof( struct  SCLL_Node));
    head->next->next = head;
    head->next->data = 20;

    displayList(head);

    struct SCLL_Node * newElement= (struct SCLL_Node * ) malloc(sizeof( struct  SCLL_Node));
    newElement->data = 0;

    int choice;
    do{
        printf( "\n==============================================================================" );
        printf( "\n1 : Insert an element into linked list before key." );
        printf( "\n2 : Insert an element into linked list after key." );
        printf( "\n3 : Accessing a node (finding the position wrt header)" );
        printf( "\n4 : Remove an element from linked list with key." );
        printf( "\n5 : Complete Deletion of list" );
        printf( "\n6 : Displaying the current list," );
        printf( "\n7 : Sorting the list." );
        printf( "\n8 : Quit" );

        printf( "\nEnter Choice : ");
        scanf(" %d", &choice);


        printf( "\n==============================================================================" );

        switch (choice) {
            case 1 : addNodeBefore(head, getElement(), getKey()); break;
            case 2 : addNodeAfter(head, getElement(), getKey()); break;
            case 3: accessingNode(head, getKey()); break;
            case 4: removeNode(head, getKey()); break;
            case 5 : deleteList(head); choice = 8 ;break;
            case 6: displayList(head); break;
//            case 7: swapNodes(&head, &(head->next)); break;
            case 8: printf("\nThank You"); deleteList(head); break;
            default: printf( "\nWrong Choice ! Please enter a valid choice;" );
        }
    } while (choice != 8);
}